<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Structure</title>
	<link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
	<style>
		li{margin: 3px 0px;}
		.required-field{color: red;}
	</style>
</head>

<body>

	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="tree-structure-wrapper pt-4">
					<?php 
					menu($all_members_tree, 0);  //call function with data array to print list
					function menu($all_members_tree, $Parent) {
					echo '<ul class="ul_'.$Parent.'">';
        			foreach ($all_members_tree as $val) {

			            if (!empty($val['Children'])) {
			                echo '<li id='.$val['Id'].'>' . $val['Name'];
			                menu($val['Children'], $val['Id']); // use recursive function for showing tree structure.
			                echo "</li>";
			            } else {
			                echo '<li id='.$val['Id'].'>' . $val['Name'] . '<ul class="ul_'.$val['Id'].'"></ul></li>'; // Creating ul for last child to append data to last position.
			            }
			        }
        			echo "</ul>";
        			}
					?>

				</div>
			</div>

			<div class="col-md-12 mt-3">
				<a id="add-member-btn" href="#add-member-modal" role="button" class="btn btn btn-info" data-toggle="modal">Add Member</a>
			</div>
			<div class="mt-2 alert alert-success d-none" id="success-alert">
                <button type="button" class="close" data-dismiss="alert">x</button>
                <strong>Success! </strong> New member added successfully.
              </div>
              <div class="alert alert-error d-none" id="error-alert">
                <strong>Error! </strong>Unable to add member.
              </div>

			<!-- Start Add Member Modal -->
			<div class="modal fade" id="add-member-modal" role="dialog" aria-labelledby="myModalLabel"
				aria-hidden="true">
				<div class="modal-dialog" role="document">
					<div class="modal-content">
						<div class="modal-header">
							<h5 class="modal-title" id="myModalLabel">
								Add Member
							</h5>
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">×</span>
							</button>
						</div>

						<!-- Start form -->
        				<form class="form-horizontal" action="javascript:void(0);">
        					<div class="modal-body">
								<div class="row">
									<div class="col-12">
										<div class="form-group">
											<label for="select-parent">Parent</label>
											<select name="Parent" class="form-control" id="select-parent">
												<option value="0">Select Parent</option>
												<?php foreach ($all_members as $value) { ?>
												<option value="<?php echo $value['Id']; ?>"><?php echo $value['Name']; ?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									<div class="col-12">
										<div class="form-group">
											<label for="member-name">Name <sup class="required-field">*</sup></label>
											<input type="text" class="form-control" name="MemberName" placeholder="Member Name" id="member-name">
											<p class="text-danger" id="error-member-name" > please enter member name.</p>
											<p class="text-danger" id="error-member-name-prop" > please enter member name in alphabets only.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" class="btn btn-primary" id="AddMemberBtn">Save changes</button>
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
							</div>
						</form>
						<!-- End of Form -->

					</div>
				</div>
			</div>
			<!-- End of Add Member Modal -->

		</div>
	</div>

	<script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
	<script type="text/javascript">

		$('#add-member-btn').click(function() {
	        $("#success-alert").addClass('d-none');
	        $("#error-alert").addClass('d-none');
	        $("#error-member-name").hide();
	        $("#error-member-name-prop").hide();
	        $("#select-parent").val(0);
	        $("#member-name").val('');

  		});

		$("#AddMemberBtn").click(function() {  //add member function click on button event
		
		var flag = true;
		var ParentID = $("#select-parent").val();
		var Name = $("#member-name").val();
// client side validation 
        if($("#member-name").val()==""){   // check not empty 
            $("#error-member-name-prop").hide();
            $("#error-member-name").show();
            flag=false;
       }else if((/^[a-zA-Z\s]+$/).test($("#member-name").val())==false){   //allow only alphabets
            $("#error-member-name").hide();
            $("#error-member-name-prop").show();
            flag=false;
       }else{
            $("#error-member-name").hide();
            $("#error-member-name-prop").hide();
        }

        if(flag == false){
            return false; 
        }
// ajax calling for add member
		$.ajax({ 
            type :'POST',
            url : '<?php echo base_url(); ?>add_member',
            data: {member_parent:ParentID, member_name:Name},
            success: function(res) {
                // console.log(res);
                $('#add-member-modal').modal('toggle');   // pop up modal close
                var obj = JSON.parse(res);     //parse the json to use object
                if(obj.Success == 1){
                	$("#select-parent").append(new Option(obj.Name, obj.Id));   //add the options to the select list
                	
                	$('.ul_'+obj.ParentId).append('<li>'+obj.Name+'<ul class="ul_'+obj.Id+'"></ul></li>');  // add the member to tree structure
                	
                    $("#success-alert").removeClass('d-none');
                    $("#success-alert").fadeTo(2000, 500).slideUp(500, function() {
                    $("#success-alert").slideUp(500);  // success message fadeout
                    });
                }else{
                	$("#error-alert").removeClass('d-none');  // in case of adding member failed error message show
                }

                },
            error: function(jqXHR, textStatus, errorThrown){
            
                }
        });
	});
</script>

</body>

</html>