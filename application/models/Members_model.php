<?php 

class Members_model extends CI_Model {


    // get data function 
    public function get_data($table_name, $where_data=array()) {

        $this->db->order_by("ParentId", "asc");    // show list of members in select dropdown as per parent priority. 

        $query = $this->db->get_where($table_name, $where_data);
        
        return ($query->num_rows() > 0) ? $query->result_array() : false;
    }

    // insert data function 
    public function insert_data($table_name, $insert_data) {

        $this->db->insert($table_name, $insert_data);
        return $this->db->insert_id();
    }

// recursive function to get tree structure members data 
     public function getTreeList($parentId=0)
    {
    $retVal = array();
    $table_name="Members";
    $where_data=array('ParentId'=>$parentId );

    $query = $this->db->get_where($table_name, $where_data);

    foreach($query->result_array() as $row)
    {
        $retVal[] = array(
       "Name" => $row["Name"],
       "Id" => $row["Id"],
       "Children" => $this->getTreeList($row["Id"])   //call recursive function
     );
    }

    return $retVal;

    }

}