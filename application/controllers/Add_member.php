<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Add_member extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Members_model');
    }
    
	public function index()
	{
         $member_data = array(   // table fields array with data to insert into dadabase
                'ParentId' => trim($this->input->post('member_parent')),
                'Name'  => trim($this->input->post('member_name'))
            );

            $insert_id = $this->Members_model->insert_data('Members',$member_data); // insert data into database function with return last member id
            if($insert_id > 0){
                $member_data = array(   // table fields array with data to return response ajax
                'ParentId' => trim($this->input->post('member_parent')),
                'Name'  => trim($this->input->post('member_name')),
                'Id'  => $insert_id,
                'Success'  => 1);
                echo json_encode($member_data);
            }else{
                echo json_encode(0);
        
            }
	}
}
