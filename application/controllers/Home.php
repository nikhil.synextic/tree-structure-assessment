<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Members_model');
    }
    
	public function index()
	{
        $data['all_members'] = $this->Members_model->get_data('Members'); //get members list
        $data['all_members_tree'] = $this->Members_model->getTreeList();  //get all members in tree structure
		$this->load->view('index', $data);   // load view page 
	}
}
